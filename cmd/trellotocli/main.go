package main

import (
	"flag"

	"github.com/olebedev/config"

	"gitlab.com/zdcca/convertcommerce/internal/nominatim"
	"gitlab.com/zdcca/convertcommerce/internal/stdout"
	"gitlab.com/zdcca/convertcommerce/internal/trello"
)

func main() {
	var configfile string
	flag.StringVar(&configfile, "c", "config.yaml", "Configuration file path (shorthand)")
	flag.StringVar(&configfile, "config", "config.yaml", "Configuration file path")
	flag.Parse()
	cfg, err := config.ParseYamlFile(configfile)
	if err != nil {
		panic(err)
	}

	labelsConf := cfg.UMap("trello.labels")
	labelsMap := map[string]string{}
	for lbl, val := range labelsConf {
		labelsMap[lbl] = val.(string)
	}
	customFieldsConf := cfg.UMap("trello.customfields")
	customFieldsMap := map[string]string{}
	for lbl, val := range customFieldsConf {
		customFieldsMap[lbl] = val.(string)
	}

	// 1. Read from Trello
	commerces, err := trello.Read(cfg.UString("trello.key"), cfg.UString("trello.token"), cfg.UString("trello.boardid"), labelsMap, customFieldsMap)
	if err != nil {
		panic(err)
	}

	err = nominatim.AddCoordinates(commerces)
	if err != nil {
		panic(err)
	}

	// 2. Write to stdout
	stdout.Write(commerces)
}
