module gitlab.com/zdcca/convertcommerce

go 1.13

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/olebedev/config v0.0.0-20190528211619-364964f3a8e4
	github.com/pkg/errors v0.8.1
	github.com/tealeg/xlsx v1.0.5
	github.com/xanzy/go-gitlab v0.20.1
	gopkg.in/yaml.v2 v2.2.4 // indirect
)
