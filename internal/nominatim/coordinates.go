package nominatim

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strconv"

	"gitlab.com/zdcca/convertcommerce/internal/zw"
)

const nominatimServer = "https://nominatim.openstreetmap.org"

// Data is data returned by a nominatim server
type Data struct {
	Latitude  string `json:"lat"`
	Longitude string `json:"lon"`
}

// AddCoordinates add coordinates to a commerce object
func AddCoordinates(commerces []*zw.Commerce) error {
	for _, c := range commerces {
		resp, err := http.Get(nominatimServer + "/search?q=" + c.Address + ", " + c.City + "&format=json")
		if err != nil {
			return err
		}
		defer resp.Body.Close()
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return err
		}
		nData := &[]*Data{}
		err = json.Unmarshal(body, nData)
		if err != nil {
			return err
		}
		if len(*nData) > 0 {
			lat, err := strconv.ParseFloat((*nData)[0].Latitude, 32)
			if err != nil {
				return err
			}
			lon, err := strconv.ParseFloat((*nData)[0].Longitude, 32)
			if err != nil {
				return err
			}
			c.Latitude = float32(lat)
			c.Longitude = float32(lon)
		}
	}
	return nil

}
