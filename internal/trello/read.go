package trello

import (
	"sort"
	"strings"
	"time"

	"gitlab.com/zdcca/convertcommerce/trello"
	// "github.com/adlio/trello"

	"gitlab.com/zdcca/convertcommerce/internal/zw"
)

type weekdayMapElement struct {
	Label string
	Day   time.Weekday
}

var weekdayMap = []weekdayMapElement{
	weekdayMapElement{"mondayhours", time.Monday},
	weekdayMapElement{"tuesdayhours", time.Tuesday},
	weekdayMapElement{"wednesdayhours", time.Wednesday},
	weekdayMapElement{"thursdayhours", time.Thursday},
	weekdayMapElement{"fridayhours", time.Friday},
	weekdayMapElement{"saturdayhours", time.Saturday},
	weekdayMapElement{"sundayhours", time.Sunday},
}

// Read returns a list of commerces from a Trello board
//
// Each list in the board which is named like "12345 Something" is considered to be a list of commerces in a city/town (French postcode+city format).
func Read(apikey, apitoken, boardid string, labelMap map[string]string, customFieldsMap map[string]string) ([]*zw.Commerce, error) {
	client := trello.NewClient(apikey, apitoken)
	board, err := client.GetBoard(boardid, trello.Defaults())
	if err != nil {
		return nil, err
	}
	bcf, err := board.GetCustomFields(trello.Defaults())
	if err != nil {
		return nil, err
	}
	lists, err := board.GetLists(trello.Defaults())
	if err != nil {
		return nil, err
	}
	commerces := []*zw.Commerce{}
	args := trello.Defaults()
	args["customFieldItems"] = "true"
	args["actions"] = "commentCard"
	for _, l := range lists {
		if !zw.IsCity(l.Name) {
			continue
		}

		cards, err := l.GetCards(args)
		if err != nil {
			return nil, err
		}
		for _, c := range cards {
			customFields := c.CustomFields(bcf)
			commerce := zw.Commerce{
				Source:      c.ShortURL,
				Name:        c.Name,
				City:        l.Name,
				Description: c.Desc,
				Comments:    make([]zw.Comment, len(c.Actions)),
			}
			addr := customFields[customFieldsMap["address"]]
			if addr != nil {
				commerce.Address = addr.(string)
			}
			url := customFields[customFieldsMap["url"]]
			if url != nil {
				commerce.URL = url.(string)
			}
			// The function asked for a type->color mapping, but reads the input data as a color->type mapping
			reverseLabels := map[string]string{}
			for lbl, val := range labelMap {
				reverseLabels[val] = lbl
			}
			for _, lbl := range c.Labels {
				labelType, ok := reverseLabels[lbl.Color]
				if !ok {
					continue
				}
				switch labelType {
				case "refused":
					commerce.Refused = true
				case "commercetype":
					commerce.Types = append(commerce.Types, lbl.Name)
				case "returnableproduct":
					commerce.Returnables = append(commerce.Returnables, lbl.Name)
				case "action":
					commerce.Actions = append(commerce.Actions, lbl.Name)
				}
			}
			sort.Strings(commerce.Types)
			sort.Strings(commerce.Returnables)
			sort.Strings(commerce.Actions)
			// Reading opening hours is a bit tricky because the input may have many different formats...
			for _, elem := range weekdayMap {
				source := customFields[customFieldsMap[elem.Label]]
				if source != nil {
					hours, err := zw.ParseOpeningHours(source.(string))
					if err != nil {
						panic(err)
					}
					commerce.OpeningPeriods = append(
						commerce.OpeningPeriods,
						zw.OpeningPeriods{
							Day:   elem.Day,
							Hours: hours,
						},
					)
				}
			}

			visitor := customFields[customFieldsMap["visitor"]]
			if visitor != nil {
				commerce.Visitor = visitor.(string)
			}
			visitDate := customFields[customFieldsMap["visitdate"]]
			if visitDate != nil {
				commerce.VisitDate = visitDate.(time.Time)
			}
			contactName := customFields[customFieldsMap["contactname"]]
			if contactName != nil {
				commerce.Contacts = append(commerce.Contacts, zw.Contact{Name: contactName.(string)})
				pos := customFields[customFieldsMap["contactposition"]]
				if pos != nil {
					commerce.Contacts[0].Position = pos.(string)
				}
				numoraddrs := customFields[customFieldsMap["contactnumoraddresses"]]
				if numoraddrs != nil {
					all := strings.Split(numoraddrs.(string), ",")
					commerce.Contacts[0].NumbersOrAddresses = make([]string, len(all))
					for i, numoraddr := range all {
						commerce.Contacts[0].NumbersOrAddresses[i] = strings.TrimSpace(numoraddr)
					}
				}
			}
			nb := customFields[customFieldsMap["nbregularstickers"]]
			if nb != nil {
				commerce.NbRegularStickers = int8(nb.(int))
			}
			nb = customFields[customFieldsMap["nbwindowstickers"]]
			if nb != nil {
				commerce.NbWindowStickers = int8(nb.(int))
			}
			nb = customFields[customFieldsMap["nbchartersigns"]]
			if nb != nil {
				commerce.NbCharterSigns = int8(nb.(int))
			}
			nb = customFields[customFieldsMap["nbjarsigns"]]
			if nb != nil {
				commerce.NbJarSigns = int8(nb.(int))
			}
			for i, action := range c.Actions {
				commerce.Comments[i] = zw.Comment{
					Author:  action.MemberCreator.Username,
					Date:    action.Date,
					Content: action.Data.Text,
				}
			}
			ok := customFields[customFieldsMap["validated"]]
			if ok != nil {
				commerce.Validated = ok.(bool)
			}
			commerces = append(commerces, &commerce)
		}
	}
	return commerces, nil
}
