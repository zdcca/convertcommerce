package xlsx

import (
	"github.com/tealeg/xlsx"

	"gitlab.com/zdcca/convertcommerce/internal/zw"
)

var (
	border            = xlsx.NewBorder("thin", "thin", "thin", "thin")
	titleStyle        = xlsx.NewStyle()
	headerStyle       = xlsx.NewStyle()
	headerStyleCenter = xlsx.NewStyle()
	cellStyle         = xlsx.NewStyle()
	cellStyleCenter   = xlsx.NewStyle()

	headerTranslate = map[string]string{
		"Eau plate / eau gazeuse": "Eaux",
	}
)

func init() {
	titleStyle.Alignment.Horizontal = "center"
	titleStyle.ApplyAlignment = true
	titleStyle.Font.Size = 14
	titleStyle.Font.Bold = true
	titleStyle.ApplyFont = true

	headerStyle.Alignment.Vertical = "center"
	headerStyle.ApplyAlignment = true
	headerStyle.Font.Bold = true
	headerStyle.ApplyFont = true
	headerStyle.Border = *border
	headerStyle.ApplyBorder = true

	headerStyleCenter.Alignment.Horizontal = "center"
	headerStyleCenter.Alignment.Vertical = "center"
	headerStyleCenter.Alignment.WrapText = true
	headerStyleCenter.ApplyAlignment = true
	headerStyleCenter.Font.Bold = true
	headerStyleCenter.ApplyFont = true
	headerStyleCenter.Border = *border
	headerStyleCenter.ApplyBorder = true

	cellStyle.Alignment.Vertical = "center"
	cellStyle.ApplyAlignment = true
	cellStyle.Border = *border
	cellStyle.ApplyBorder = true

	cellStyleCenter.Alignment.Horizontal = "center"
	cellStyleCenter.Alignment.Vertical = "center"
	cellStyleCenter.Alignment.WrapText = true
	cellStyleCenter.ApplyAlignment = true
	cellStyleCenter.Border = *border
	cellStyleCenter.ApplyBorder = true
}

// Write writes the commerces list as CSV in a file
func Write(commerces []*zw.Commerce, path string) error {

	returnables := []string{}

	for _, c := range commerces {
	loop_on_returnables:
		for _, r := range c.Returnables {
			for _, globalR := range returnables {
				if r == globalR {
					continue loop_on_returnables
				}
			}
			returnables = append(returnables, r)
		}
	}

	file := xlsx.NewFile()
	sheet, err := file.AddSheet("Commerçants")
	if err != nil {
		return err
	}
	makeHeaders(sheet, returnables)

	for _, c := range commerces {
		if c.Validated {
			makeRow(sheet, returnables, c)
		}
	}

	if err := sheet.SetColWidth(0, 0, 25); err != nil {
		return err
	}
	if err := sheet.SetColWidth(1, 1, 20); err != nil {
		return err
	}
	if err := sheet.SetColWidth(2, 2, 15); err != nil {
		return err
	}
	if err := sheet.SetColWidth(3, 14, 3); err != nil {
		return err
	}
	if err := sheet.SetColWidth(15, 17, 13); err != nil {
		return err
	}

	return file.Save(path)
}

func makeHeaders(sheet *xlsx.Sheet, returnables []string) {
	sheet.AddRow()
	row2 := sheet.AddRow()
	a2 := row2.AddCell()
	a2.Value = "Liste des commerçants engagés dans l'opération \"Mon commerçant zéro déchet\" à ce jour"
	a2.Merge(17, 0)
	a2.SetStyle(titleStyle)
	row2.SetHeight(20)
	sheet.AddRow()
	row4 := sheet.AddRow()
	a4 := row4.AddCell()
	a4.Value = "Commerce"
	b4 := row4.AddCell()
	b4.Value = "Adresse"
	c4 := row4.AddCell()
	c4.Value = "Commune"
	d4 := row4.AddCell()
	row4.AddCell()
	row4.AddCell()
	row4.AddCell()
	row4.AddCell()
	row4.AddCell()
	row4.AddCell()
	row4.AddCell()
	row4.AddCell()
	row4.AddCell()
	row4.AddCell()
	row4.AddCell()
	d4.Value = "Engagements"
	d4.Merge(11, 0)
	p4 := row4.AddCell()
	p4.Value = "Produits vendus en contenants consignés"
	for i := 0; i < len(returnables)-1; i++ {
		c := row4.AddCell()
		c.SetStyle(cellStyle)
	}
	p4.Merge(len(returnables)-1, 0)
	row5 := sheet.AddRow()
	a5 := row5.AddCell()
	a4.Merge(0, 1)
	b5 := row5.AddCell()
	b4.Merge(0, 1)
	c5 := row5.AddCell()
	c4.Merge(0, 1)
	d5 := row5.AddCell()
	d5.Value = "1"
	e5 := row5.AddCell()
	e5.Value = "2"
	f5 := row5.AddCell()
	f5.Value = "3"
	g5 := row5.AddCell()
	g5.Value = "4"
	h5 := row5.AddCell()
	h5.Value = "5"
	i5 := row5.AddCell()
	i5.Value = "6"
	j5 := row5.AddCell()
	j5.Value = "7"
	k5 := row5.AddCell()
	k5.Value = "8"
	l5 := row5.AddCell()
	l5.Value = "9"
	m5 := row5.AddCell()
	m5.Value = "10"
	n5 := row5.AddCell()
	n5.Value = "11"
	o5 := row5.AddCell()
	o5.Value = "12"
	a4.SetStyle(headerStyle)
	b4.SetStyle(headerStyle)
	c4.SetStyle(headerStyle)
	a5.SetStyle(headerStyle)
	b5.SetStyle(headerStyle)
	c5.SetStyle(headerStyle)
	d4.SetStyle(headerStyleCenter)
	p4.SetStyle(headerStyleCenter)
	d5.SetStyle(headerStyleCenter)
	e5.SetStyle(headerStyleCenter)
	f5.SetStyle(headerStyleCenter)
	g5.SetStyle(headerStyleCenter)
	h5.SetStyle(headerStyleCenter)
	i5.SetStyle(headerStyleCenter)
	j5.SetStyle(headerStyleCenter)
	k5.SetStyle(headerStyleCenter)
	l5.SetStyle(headerStyleCenter)
	m5.SetStyle(headerStyleCenter)
	n5.SetStyle(headerStyleCenter)
	o5.SetStyle(headerStyleCenter)
	row4.SetHeight(15)
	row5.SetHeight(30)
	for _, r := range returnables {
		c := row5.AddCell()
		mapped, ok := headerTranslate[r]
		if ok {
			c.Value = mapped
		} else {
			c.Value = r
		}
		c.SetStyle(headerStyleCenter)
	}
}

var rowPrefix = map[string]int{"01": 0, "02": 1, "03": 2, "04": 3, "05": 4, "06": 5, "07": 6, "08": 7, "09": 8, "10": 9, "11": 10, "12": 11}

func makeRow(sheet *xlsx.Sheet, returnables []string, c *zw.Commerce) {
	row := sheet.AddRow()
	cell := row.AddCell()
	cell.Value = c.Name
	cell.SetStyle(headerStyle)
	cell = row.AddCell()
	cell.Value = c.Address
	cell.SetStyle(cellStyle)
	cell = row.AddCell()
	cell.Value = c.City
	cell.SetStyle(cellStyle)
	actions := [12]bool{}
	for _, a := range c.Actions {
		actions[rowPrefix[a[:2]]] = true
	}
	for _, v := range actions {
		cell = row.AddCell()
		if v {
			cell.Value = "X"
		}
		cell.SetStyle(cellStyleCenter)
	}
	for _, ret := range returnables {
		cell = row.AddCell()
		for _, r := range c.Returnables {
			if r == ret {
				cell.Value = "oui"
				break
			}
		}
		cell.SetStyle(cellStyleCenter)
	}
	row.SetHeight(15)
}
