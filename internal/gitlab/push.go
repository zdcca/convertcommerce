package gitlab

import (
	"crypto/md5"

	"github.com/xanzy/go-gitlab"
)

// Push pushes a textual content to a file in a branch of a repository
//
// * token is the GitLab API token
// * repository is the GitLab repository (<group>/<repository>)
// * branch is the branch where to update the file
// * filename is the path to the file
// * content is the file's content, as a string
func Push(token, repository, branch, filename, content string) error {
	git := gitlab.NewClient(nil, token)

	f, _, err := git.RepositoryFiles.GetRawFile(repository, filename, &gitlab.GetRawFileOptions{
		Ref: &branch,
	})
	if err != nil {
		return err
	}
	newSum := md5.Sum([]byte(content))
	currentSum := md5.Sum(f)

	// The file is the same, do not upload
	if newSum == currentSum {
		return nil
	}

	commitMessage := "Auto push for commerces list"
	_, _, err = git.RepositoryFiles.UpdateFile(repository, filename, &gitlab.UpdateFileOptions{
		Branch:        &branch,
		Content:       &content,
		CommitMessage: &commitMessage,
	})
	return err
}
