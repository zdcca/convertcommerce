package csv

import (
	"encoding/csv"
	"os"
	"strconv"
	"strings"

	"gitlab.com/zdcca/convertcommerce/internal/zw"
)

var headers = []string{
	"Date visite",
	"Commerçant",
	"Description",
	"Nom contact",
	"Poste contact",
	"Coordonnées contact",
	"Types de commerce",
	"Produits consignés",
	"Qt stickers base",
	"Qt stickers vitro",
	"Qt affiches bocal",
	"Qt affiches charte",
	"Actions",
	"Observations",
}

// Write writes the commerces list as CSV in a file
func Write(commerces []*zw.Commerce, path string) error {
	f, err := os.Create(path)
	if err != nil {
		return err
	}
	defer f.Close()
	w := csv.NewWriter(f)
	defer w.Flush()
	w.Write(headers)
	for _, c := range commerces {
		w.Write(asRow(c))
	}
	return nil
}

func asRow(c *zw.Commerce) []string {
	row := make([]string, 14)
	row[0] = c.VisitDate.Format("02/01/2006")
	row[1] = c.Name + "\n" + c.Address + "\n" + c.City
	row[2] = c.Description
	if len(c.Contacts) > 0 {
		row[3] = c.Contacts[0].Name
		row[4] = c.Contacts[0].Position
		row[5] = strings.Join(c.Contacts[0].NumbersOrAddresses, "\n")
	}
	row[6] = strings.Join(c.Types, "\n")
	row[7] = strings.Join(c.Returnables, "\n")
	row[8] = strconv.Itoa(int(c.NbRegularStickers))
	row[9] = strconv.Itoa(int(c.NbWindowStickers))
	row[10] = strconv.Itoa(int(c.NbJarSigns))
	row[11] = strconv.Itoa(int(c.NbCharterSigns))
	row[12] = strings.Join(c.Actions, "\n")
	comments := make([]string, len(c.Comments))
	for i, comment := range c.Comments {
		comments[i] = comment.Author + " le " + comment.Date.Format("02/01/2006 à 15:04") + " :\n\n" + comment.Content
	}
	row[13] = strings.Join(comments, "\n----------\n")
	return row
}
