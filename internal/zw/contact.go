package zw

// Contact defines a person
type Contact struct {
	Name               string
	Position           string
	NumbersOrAddresses []string
}
