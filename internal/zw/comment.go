package zw

import "time"

// Comment defines a comment...
type Comment struct {
	Author  string
	Date    time.Time
	Content string
}
