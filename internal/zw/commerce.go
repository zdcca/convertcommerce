package zw

import (
	"strings"
	"time"
)

// Commerce is a references commerce
type Commerce struct {
	Source string

	Refused bool // If this bool is true, the commerce does not want to participate

	Name    string
	Address string
	City    string
	URL     string

	Types       []string
	Returnables []string

	OpeningPeriods []OpeningPeriods

	Actions []string

	Description string

	Latitude  float32
	Longitude float32

	// Attributes below this line are for internal use only

	Visitor   string
	VisitDate time.Time

	Contacts []Contact

	NbRegularStickers int8
	NbWindowStickers  int8
	NbCharterSigns    int8
	NbJarSigns        int8

	Comments []Comment

	Validated bool
}

// FormatURL returns the formatted URL (to make sure it starts with "http://" or "https://")
func (c Commerce) FormatURL() string {
	if c.URL == "" {
		return ""
	}
	if strings.HasPrefix(c.URL, "http://") || strings.HasPrefix(c.URL, "https://") {
		return c.URL
	}
	return "http://" + c.URL
}
