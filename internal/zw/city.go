package zw

import (
	"regexp"
)

var cityRe = regexp.MustCompile("[0-9][0-9][0-9][0-9][0-9] .*")

// IsCity returns true if the given string designates a french city (postcode and city name)
func IsCity(n string) bool {
	return cityRe.MatchString(n)
}
