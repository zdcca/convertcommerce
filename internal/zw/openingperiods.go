package zw

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"
	"time"
)

// OpeningPeriods defines opening periods for a weekday
type OpeningPeriods struct {
	Day   time.Weekday
	Hours []OpeningHours
}

// OpeningHours defines an opening period start and end
type OpeningHours struct {
	FromHour   int8
	FromMinute int8
	ToHour     int8
	ToMinute   int8
}

// WeekdayI18n contains translations for week days
var WeekdayI18n = map[string][]string{
	"fr": []string{
		"Dimanche",
		"Lundi",
		"Mardi",
		"Mercredi",
		"Jeudi",
		"Vendredi",
		"Samedi",
	},
}

// String returns the opening hours as a string
func (o OpeningHours) String() string {
	return fmt.Sprintf("%02d:%02d-%02d:%02d", o.FromHour, o.FromMinute, o.ToHour, o.ToMinute)
}

// String returns the opening perids for a day as a string
func (o OpeningPeriods) String(lang string) string {
	var day string
	translation, ok := WeekdayI18n[lang]
	if ok {
		day = translation[o.Day]
	} else {
		day = o.Day.String() // Default translation by default...
	}
	allHours := make([]string, len(o.Hours))
	for i, hours := range o.Hours {
		allHours[i] = hours.String()
	}
	return day + ": " + strings.Join(allHours, ", ")
}

var spaceDash = regexp.MustCompile(" *- *")

// ParseOpeningHours parses an input string and searches for opening hours
//
// A string may contain multiple period definitions, separated by any number of spaces (" ") or commas(",") or both (", ")
//
// A period definition must contain two timestamps, separated by a dash ("-"), surrounded by spaces or not (" - " is accepted, for instance)
//
// A timestamp must contain an hour and may contain a minute, separated by a column (":") or the letter H ("h" or "H")
//
// Example of accepted inputs, all having the same meaning:
//
//  9-12:30 14-18h30
//  09:00 - 12:30,   14h00 -18h30
//  9H-12H30,14H-18H30
//  09:00-12:00 14:00-18:30
func ParseOpeningHours(source string) ([]OpeningHours, error) {
	result := []OpeningHours{}
	source = strings.ReplaceAll(source, ",", " ")
	source = spaceDash.ReplaceAllString(source, "-")
	for _, pSource := range strings.Fields(source) {
		hours := strings.SplitN(pSource, "-", 2)
		if len(hours) != 2 {
			return nil, fmt.Errorf("Incorrect period definition: '%s'", pSource)
		}
		frH, frM, err := parseHour(hours[0])
		if err != nil {
			return nil, err
		}
		toH, toM, err := parseHour(hours[1])
		if err != nil {
			return nil, err
		}
		if toH == 0 && toM == 0 {
			toH = 23
			toM = 59
		}
		result = append(result, OpeningHours{
			FromHour:   frH,
			FromMinute: frM,
			ToHour:     toH,
			ToMinute:   toM,
		})
	}
	return result, nil
}

func hourSep(r rune) bool {
	return r == ':' || r == 'h' || r == 'H'
}
func parseHour(source string) (int8, int8, error) {
	fields := strings.FieldsFunc(source, hourSep)
	if len(fields) > 2 {
		return 0, 0, fmt.Errorf("Wrong format for an hour: '%s'", source)
	}
	if len(fields) == 0 {
		fields = []string{"0"}
	}
	hour, err := strconv.Atoi(fields[0])
	if err != nil {
		return 0, 0, err
	}
	if hour > 23 {
		return 0, 0, fmt.Errorf("An hour cannot be larger than 23 ('%s')", fields[0])
	}
	var minute int
	if len(fields) == 2 {
		minute, err = strconv.Atoi(fields[1])
		if err != nil {
			return 0, 0, err
		}
		if minute > 59 {
			return 0, 0, fmt.Errorf("A minute cannot be larger than 59 ('%s')", fields[1])
		}
	}
	return int8(hour), int8(minute), nil
}
