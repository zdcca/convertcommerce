package json

import (
	"encoding/json"
	"fmt"
	"sort"
	"strings"

	"gitlab.com/zdcca/convertcommerce/internal/zw"
)

// ResultData is the format of the data that will be created
type ResultData struct {
	Commerces   []Commerce `json:"commerces"`
	Cities      []Backlink `json:"cities"`
	Types       []Backlink `json:"types"`
	Returnables []Backlink `json:"returnables"`
	Actions     []Backlink `json:"actions"`
}

// Commerce is a single commerce in the resulting data
type Commerce struct {
	Name           string   `json:"name"`
	Address        string   `json:"address,omitempty"`
	City           string   `json:"city"`
	URL            string   `json:"url,omitempty"`
	Types          []string `json:"types,omitempty"`
	Returnables    []string `json:"returnables,omitempty"`
	OpeningPeriods []string `json:"openingperiods,omitempty"`
	Actions        []string `json:"actions,omitempty"`
	Description    string   `json:"description,omitempty"`
	Latitude       float32  `json:"lat,omitempty"`
	Longitude      float32  `json:"lon,omitempty"`
}

// Backlink makes a link for some detail to the list of all corresponding commerces
type Backlink struct {
	Name string `json:"name"`
	In   []int  `json:"in"`
}

// String returns the publishable commerces list in JSON format as a string
func String(commerces []*zw.Commerce) (string, error) {
	var (
		result = ResultData{
			Commerces:   []Commerce{},
			Cities:      []Backlink{},
			Types:       []Backlink{},
			Returnables: []Backlink{},
			Actions:     []Backlink{},
		}

		allCities      = map[string][]int{}
		allTypes       = map[string][]int{}
		allReturnables = map[string][]int{}
		allActions     = map[string][]int{}
		i              = 0
	)
	for _, c := range commerces {
		if !c.Validated || c.Refused {
			continue
		}
		current := Commerce{
			Name:           c.Name,
			Address:        c.Address,
			City:           c.City,
			URL:            c.FormatURL(),
			Types:          c.Types,
			Returnables:    c.Returnables,
			OpeningPeriods: make([]string, len(c.OpeningPeriods)),
			Actions:        c.Actions,
			Description:    c.Description,
			Latitude:       c.Latitude,
			Longitude:      c.Longitude,
		}
		for j, o := range c.OpeningPeriods {
			current.OpeningPeriods[j] = o.String("fr")
		}
		allCities[c.City] = append(allCities[c.City], i)
		for _, t := range c.Types {
			allTypes[t] = append(allTypes[t], i)
		}
		for _, r := range c.Returnables {
			allReturnables[r] = append(allReturnables[r], i)
		}
		for _, a := range c.Actions {
			allActions[a] = append(allActions[a], i)
		}
		result.Commerces = append(result.Commerces, current)
		i++
	}
	result.Cities = mapToBacklinks(allCities)
	result.Types = mapToBacklinks(allTypes)
	result.Returnables = mapToBacklinks(allReturnables)
	result.Actions = mapToBacklinks(allActions)
	sort.Slice(result.Commerces, func(i, j int) bool {
		a := result.Commerces[i]
		b := result.Commerces[j]
		// Sort by city...
		diff := strings.Compare(a.City, b.City)
		switch diff {
		case -1:
			return true
		case 1:
			return false
		}
		// ... and by name
		return strings.Compare(a.Name, b.Name) <= 0
	})
	res, err := json.MarshalIndent(result, "", "  ")
	if err != nil {
		return "", err
	}
	return string(res), nil
}

// Write writes the commerces list on the standard output
func Write(commerces []*zw.Commerce) error {
	s, err := String(commerces)
	if err != nil {
		return err
	}
	fmt.Println(s)
	return nil
}

func mapToBacklinks(source map[string][]int) []Backlink {
	length := len(source)
	keys := make([]string, length)
	i := 0
	for k := range source {
		keys[i] = k
		i++
	}
	sort.Strings(keys)
	result := make([]Backlink, length)
	for i, k := range keys {
		result[i] = Backlink{
			Name: k,
			In:   source[k],
		}
	}
	return result
}
