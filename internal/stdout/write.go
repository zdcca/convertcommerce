package stdout

import (
	"fmt"
	"os"
	"text/template"

	"gitlab.com/zdcca/convertcommerce/internal/zw"
)

// TODO Extract french titles to conf or i18n sublib
var tmpl = template.Must(template.New("zw").Parse(
	`   {{ .Name }} ({{ .Source }})
   {{ .Address }}
   {{ .City }}
   {{ .FormatURL }}
{{- with .Types }}

Type de commerce:
{{- range . }}
* {{ . }}
{{- end }}
{{- end }}
{{- with .Returnables }}

Consignes:
{{- range . }}
* {{ . }}
{{- end }}
{{- end }}
{{- with .OpeningPeriods }}

Ouverture :
{{- range . }}
* {{ .String "fr" }}
{{- end }}
{{- end }}
{{- with .Actions }}

Actions :
{{- range . }}
* {{ . }}
{{- end }}

{{- end }}
{{- with .Description }}

{{ . }}
{{- end }}

---

Vu par {{ .Visitor }}, le {{ .VisitDate.Format "02/01/2006" }}
{{- with .Contacts }}

Contacts :
{{- range . }}
* {{ .Name }}, {{ .Position }}
{{- range .NumbersOrAddresses }}
  * {{ . }}
{{- end }}
{{- end }}
{{- end }}

Laissé sur place :
{{- with .NbRegularStickers }}
* {{ . }} sticker{{ if gt . 1 }}s{{ end }} simple{{ if gt . 1 }}s{{ end }}
{{- end }}
{{- with .NbWindowStickers }}
* {{ . }} sticker{{ if gt . 1 }}s{{ end }} vitrophanie
{{- end }}
{{- with .NbCharterSigns }}
* {{ . }} affiche{{ if gt . 1 }}s{{ end }} charte
{{- end }}
{{- with .NbJarSigns }}
* {{ . }} affiche{{ if gt . 1 }}s{{ end }} bocal
{{- end }}
{{- with .Comments }}

Commentaires:
----------
{{- range . }}
     {{ .Author }} le {{ .Date.Format "02/01/2006 à 15:04" }}

{{ .Content }}
----------
{{- end }}
{{- end }}
{{- if .Validated }}

Validé!{{ end }}

--------------------`,
))

// Write writes the commerces list on the standard output
func Write(commerces []*zw.Commerce) {
	for _, c := range commerces {
		tmpl.Execute(os.Stdout, c)
	}
	fmt.Println("") // CRLF at the end
}
