# Convertcommerce for ZW commerces

## Context

This Golang project is created by Sébastien Maccagnoni for *Zéro Déchet Colmar et Centre Alsace* (part of *Zero Waste France*) for the "Mon commerçant Zéro Déchet" (translatable to *"My Zero Waste Merchant"*) project.

This national project aims to identify commerces acting positively towards Zero Waste initiatives. The minimum action for these commerces is to accept personal containers (like boxes and jars) instead of providing plastic or paper wrapping or packaging.

At [Zéro Déchet Colmar et Centre Alsace](https://zdcca.fr), we list these commerces in a [Trello](https://trello.com/) board, and publish them on our website. We needed some way to convert the Trello listing to something usable on the website. This website is hosted by [Netlify](https://www.netlify.com/), which publishes a new version of the site whenever a new commit happens on the linked *git* repository (which, in our case, is hosted by [GitLab](https://gitlab.com/)).

## The "trello" package

The `trello` directory is a copy of the https://github.com/adlio/trello/pull/54 pull request. When it will be merged into the `github.com/adlio/trello` package, this directory will be removed.

## Internal packages

Packages in the `internal` directory provide the following functions:

* `gitlab.Push`: push a new file to a *GitLab* repository, only if it has changed
* `json.String`: transform commerces data to a json object (only public data)
* `json.Write`: write commerces data as a json object (only public data) to stdout
* `nominatim.AddCoordinates`: add the GPS coordinates to all commerces in a commerces list, using a Nominatim service
* `stdout.Write`: write commerces data (including private data) to stdout, human-readable
* `trello.Read`: read data from a Trello board (to be documented if needed)

The `internal/zw` package contains data structures for transitionary data.

## Commands

Packages in the `cmd` directory provide the following executables:

* `trellotojson`: convert the Trello data to JSON data and upload it to a GitLab repository, as a one-shot action